<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$barang = mysqli_query($koneksi, "SELECT * FROM barang");

	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($koneksi, "SELECT * FROM barang where nama_barang like '%$key%'");
	}
	else {
		$cari = $barang;
	}
?>
<div class="container" style="margin-top:40px">
	<h2>Daftar Barang</h2>
	<hr>	
	<table class="table table-striped table-hover table-sm table-bordered">
			<main role="main" class="col-md-9 col-lg-12 px-3">
				<form method="get" class="ml-2 mt-3">
					<label for="formGroupExampleInput">Pencarian Nama Barang</label>
					<div class="input-group mb-3 w-100">
					    <input type="text" class="form-control" name="cari" placeholder="Cari">
					    <div class="input-group-apend">
						    <input type="submit"class="btn btn-primary">
						</div>
					</div>
					<table class="table table-striped table-sm w-100 p-3 ml-1 mt-3">
					<tr>
						<td>Tota Data</td>
						<td scope="row">:</td>
						<td scope="row">
							<?php echo $total = mysqli_num_rows($cari);?>
						</td>
					</tr>
				</table>
				</form>
				<table class="table table-bordered w-100 p-3 ml-1 mt-3">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Id Barang</th>
							<th scope="col">Id Kategori</th>
							<th scope="col">Nama Barang</th>
							<th scope="col">Warna</th>
							<th scope="col">Ukuran</th>
							<th scope="col">Harga</th>
							<th scope="col">Id Pembeli</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($cari as $value):?>
						<tr>
							<th scope="row"><?php echo $value['id_barang']; ?></th>
							<td><?php echo $value['id_kategori']; ?></td>
							<td><?php echo $value['nama_barang']; ?></td>
							<td><?php echo $value['warna']; ?></td>
							<td><?php echo $value['ukuran']; ?></td>
							<td><?php echo $value['harga']; ?></td>
							<td><?php echo $value['id_pembeli']; ?></td>
							<td>
								<a href="edit_barang.php?id=<?php echo $value['id_barang'] ?>" class ="badge badge-warning">Edit</a>
								<a href="hapus_barang.php?id=<?php echo $value['id_barang'] ?>" class="badge badge-danger">Hapus</a>
								<a href="tambah_barang.php" class ="badge badge-warning">Tambah Data</a>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</main>
		</div>
	</div>
</div>
<?php 
	
	include 'layout/footer.php';

?>