<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$id = $_GET['id'];
	$data = mysqli_query($koneksi, "SELECT * FROM barang where id_barang = '$id'");

	foreach($data as $value):

?>
	
<div class="container" style="margin-top:40px">
	<h3>Ubah Data Pelanggan</h3>

	<form method="post" action="ubah_barang.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Barang</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_barang" value="<?php echo $value['id_barang'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Kategori</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_kategori" value="<?php echo $value['id_kategori'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Nama Barang</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="nama_barang" value="<?php echo $value['nama_barang'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Warna</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="warna" value="<?php echo $value['warna'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Ukuran</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="ukuran" value="<?php echo $value['ukuran'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Harga</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="harga" value="<?php echo $value['harga'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_pembeli" value="<?php echo $value['id_pembeli'] ?>">
			</div>
		</div>
		<div class="col-sm-10">
			<input type="submit" name="submit" class="btn btn-primary" value="SAVE">
			<a href="barang.php" class="btn btn-warning">KEMBALI</a>
		</div>
	</form>
</div>


<?php 
	endforeach;
	
	include 'layout/footer.php';

?>