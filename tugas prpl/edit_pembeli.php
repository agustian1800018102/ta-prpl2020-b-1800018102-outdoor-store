<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$id = $_GET['id'];
	$data = mysqli_query($koneksi, "SELECT * FROM pembeli where id_pembeli = '$id'");

	foreach($data as $value):

?>
	
<div class="container" style="margin-top:40px">
	<h3>Ubah Data Pembeli</h3>

	<form method="post" action="ubah_pembeli.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_pembeli" value="<?php echo $value['id_pembeli'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Nama Pembeli</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="nama_pembeli" value="<?php echo $value['nama_pembeli'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Alamat</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="alamat" value="<?php echo $value['alamat'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pegawai</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_pegawai" value="<?php echo $value['id_pegawai'] ?>">
			</div>
		</div>
		<div class="col-sm-10">
			<input type="submit" name="submit" class="btn btn-primary" value="SAVE">
			<a href="pembeli.php" class="btn btn-warning">KEMBALI</a>
		</div>
	</form>
</div>


<?php 
	endforeach;
	
	include 'layout/footer.php';

?>