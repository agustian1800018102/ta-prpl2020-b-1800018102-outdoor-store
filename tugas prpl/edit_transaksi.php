<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$id = $_GET['id'];
	$data = mysqli_query($koneksi, "SELECT * FROM transaksi where id_transaksi = '$id'");

	foreach($data as $value):

?>
	
<div class="container" style="margin-top:40px">
	<h3>Ubah Data Pelanggan</h3>

	<form method="post" action="ubah_transaksi.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Transaksi</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_transaksi" value="<?php echo $value['id_transaksi'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Barang</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_barang" value="<?php echo $value['id_barang'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_pembeli" value="<?php echo $value['id_pembeli'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Tanggal transaksi</label>
			<div class="col-sm-10">
				<input type="date" name="tgl_transaksi" class="form-control">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jumlah</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="jumlah" value="<?php echo $value['jumlah'] ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Total</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="total" value="<?php echo $value['total'] ?>">
			</div>
		</div>
		<div class="col-sm-10">
			<input type="submit" name="submit" class="btn btn-primary" value="SAVE">
			<a href="transaksi.php" class="btn btn-warning">KEMBALI</a>
		</div>
	</form>
</div>


<?php 
	endforeach;
	
	include 'layout/footer.php';

?>