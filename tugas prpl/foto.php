<!DOCTYPE html>
<html lang="en">
<head>
  <title>OUTDOOR STORE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>

    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }

.dropbtn {
  background-color: black;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}

.dropdown:hover .dropbtn {
  background-color: #91B3BC;
}

  </style>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="toggle navigation">
        <span class="navbar-toggler-icon"></span>
     </button>
          <a class="navbar-brand" href="foto.php">OUTDOOR STORE</a>
    </div>
        <div class="dropdown">
        <button class="dropbtn">CATEGORY</button>
        <div class="dropdown-content">
          <a href="mount.php">MOUNTENNERING</a>
          <a href="style.php">STYLE 1989</a>
          <a href="riding.php">RIDING</a>
  </div>
</div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="data.php"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
      </ul>
    </div>
  </div>
</nav>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>
      <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="1.jpg" style="width:100%; height: 800px" alt="Image">
        <div class="carousel-caption">
        </div>      
      </div>

      <div class="item">
        <img src="c.jpg" style="width:100%; height: 800px" alt="Image">
        <div class="carousel-caption">
        </div>      
      </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    <br>
</div>
   <div style="text-align: center;" class="col-sm-4">
      <h4 align="center">RIDING</h4>
      <br>
      <img src="e.jpg" style="width:50%" alt="Image">
      <p align="center">Eiger Riding hadir untuk Anda </p>
      <p>yang gemar berkendara roda dua</p>
      <p>yang didesain khusus untuk memaksimalkan kenyamanan</p>
      <p>dan keamanan dalam rutinitas berkendara.</p>
    </div>
    <div style="text-align: center;" class="col-sm-4"> 
      <h4>STYLE 1989</h4>
     <br>
      <img src="f.jpg" style="width:50%" alt="Image">
      <p>Authentic 1989 diinspirasi dari gaya</p>
      <p>klasik para petualang alam terbuka yang didesain</p>
      <p>kasual dan stylish untuk kegiatan sehari-hari dan traveling.</p>
    </div>
    <div style="text-align: center;" class="col-sm-4"> 
    <h4 align="center">MOUNTENNERING</h4>    
      <br>
      <img src="g.jpg" style="width:50%" alt="Image">
      <p>Mountaineering dirancang khusus dengan</p>
      <p>berbagai teknologi dan fitur-fitur inovatif</p>
      <p>untuk mendukung kegiatan petualangan luar ruang di iklim tropis.</p>
    </div>
<footer class="container-fluid text-center">
  <p>Toko Outdoor 2020</p> 
</footer>

</body>
</html>
