<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$kategori = mysqli_query($koneksi, "SELECT * FROM kategori");

	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($koneksi, "SELECT * FROM kategori where nama_kategori like '%$key%'");
	}
	else {
		$cari = $kategori;
	}
?>

<div class="container" style="margin-top:40px">
	<h2>Daftar Kategori</h2>
	<hr>	
	<table class="table table-striped table-hover table-sm table-bordered">
			<main role="main" class="col-md-9 col-lg-12 px-3">
				<form method="get" class="ml-2 mt-3">
					<label for="formGroupExampleInput">Pencarian Nama Kategori</label>
					<div class="input-group mb-3 w-100">
					    <input type="text" class="form-control" name="cari" placeholder="Cari">
					    <div class="input-group-apend">
						    <input type="submit"class="btn btn-primary">
						</div>
					</div>
					<table class="table table-striped table-sm w-100 p-3 ml-1 mt-3">
					<tr>
						<td>Total Data</td>
						<td scope="row">:</td>
						<td scope="row">
							<?php echo $total = mysqli_num_rows($cari);?>
						</td>
					</tr>
				</table>
				</form>

				<table class="table table-bordered w-100 p-3 ml-1 mt-3">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Id kategori</th>
							<th scope="col">Nama kategori</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($cari as $value):?>
						<tr>
							<th scope="row"><?php echo $value['id_kategori']; ?></th>
							<td><?php echo $value['nama_kategori']; ?></td>
							<td>
								<a href="edit_kategori.php?id=<?php echo $value['id_kategori'] ?>" class ="badge badge-warning">Edit</a>
								<a href="hapus_kategori.php?id=<?php echo $value['id_kategori'] ?>" class="badge badge-danger">Hapus</a>
								<a href="tambah_kategori.php?id=" class ="badge badge-warning">Tambah Data</a>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</main>
		</div>
	</div>
</div>
   <div class="col-sm-4">
      <img src="e.jpg" class="img-responsive" style="width:100%" alt="Image">
      <br>
      <p class ="badge badge-warning">RIDING</p>
    </div>
    <div class="col-sm-4"> 
      <img src="f.jpg" class="img-responsive" style="width:100%" alt="Image">
      <br>
      <p class ="badge badge-warning">STYLE 1989</p>    
    </div>
    <div class="col-sm-4"> 
      <img src="g.jpg" class="img-responsive" style="width:100%" alt="Image">
      <br>
      <p class ="badge badge-warning">MOUNTENNERING</p>    
    </div>
<?php 
	
	include 'layout/footer.php';

?>