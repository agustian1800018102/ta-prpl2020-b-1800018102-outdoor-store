<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$pegawai = mysqli_query($koneksi, "SELECT * FROM pegawai");

	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($koneksi, "SELECT * FROM pegawai where nama_pegawai like '%$key%'");
	}
	else {
		$cari = $pegawai;
	}
?>
<div class="container" style="margin-top:40px">
	<h2>Daftar Pegawai</h2>
	<hr>	
	<table class="table table-striped table-hover table-sm table-bordered">
			<main role="main" class="col-md-9 col-lg-12 px-3">
				<form method="get" class="ml-2 mt-3">
					<label for="formGroupExampleInput">Pencarian Nama Petugas</label>
					<div class="input-group mb-3 w-100">
					    <input type="text" class="form-control" name="cari" placeholder="Cari">
					    <div class="input-group-apend">
						    <input type="submit"class="btn btn-primary">
						</div>
					</div>
					<table class="table table-striped table-sm w-100 p-3 ml-1 mt-3">
					<tr>
						<td>Total Data</td>
						<td scope="row">:</td>
						<td scope="row">
							<?php echo $total = mysqli_num_rows($cari);?>
						</td>
					</tr>
				</table>
				</form>

				<table class="table table-bordered w-100 p-3 ml-1 mt-3">
					<thead class="thead-dark">
						<tr>
							<th scope="col">id Pegawai</th>
							<th scope="col">Nama Pegawai</th>
							<th scope="col">Alamat</th>
							<th scope="col">Jabatan</th>
							<th scope="col">Jenis Kelamin</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody> 
						<?php foreach($cari as $value):?>
						<tr>
							<th scope="row"><?php echo $value['id_pegawai']; ?></th>
							<td><?php echo $value['nama_pegawai']; ?></td>
							<td><?php echo $value['alamat']; ?></td>
							<td><?php echo $value['jabatan']; ?></td>
							<td><?php echo $value['jenis_kelamin']; ?></td>
							<td>
								<a href="edit_pegawai.php?id=<?php echo $value['id_pegawai'] ?>" class ="badge badge-warning">Edit</a>
								<a href="hapus_pegawai.php?id=<?php echo $value['id_pegawai'] ?>"class="badge badge-danger">Hapus</a>
								<a href="tambah_pegawai.php" class ="badge badge-warning">Tambah Petugas</a>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</main>
		</div>
	</div>
</div>
<?php 
	
	include 'layout/footer.php';

?>