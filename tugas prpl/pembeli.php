<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$pembeli = mysqli_query($koneksi, "SELECT * FROM pembeli");

	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($koneksi, "SELECT * FROM pembeli where nama_pembeli like '%$key%'");
	}
	else {
		$cari = $pembeli;
	}
?>

<div class="container" style="margin-top:40px">
	<h2>Daftar Pembeli</h2>
	<hr>	
	<table class="table table-striped table-hover table-sm table-bordered">
			<main role="main" class="col-md-9 col-lg-12 px-3">
				<form method="get" class="ml-2 mt-3">
					<label for="formGroupExampleInput">Pencarian Nama Pembeli</label>
					<div class="input-group mb-3 w-100">
					    <input type="text" class="form-control" name="cari" placeholder="Cari">
					    <div class="input-group-apend">
						    <input type="submit"class="btn btn-primary">
						</div>
					</div>
					<table class="table table-striped table-sm w-100 p-3 ml-1 mt-3">
					<tr>
						<td>Total Data</td>
						<td scope="row">:</td>
						<td scope="row">
							<?php echo $total = mysqli_num_rows($cari);?>
						</td>
					</tr>
				</table>
				</form>

				<table class="table table-bordered w-100 p-3 ml-1 mt-3">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Id Pembeli</th>
							<th scope="col">Nama Pembeli</th>
							<th scope="col">Alamat</th>
							<th scope="col">Id Pegawai</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($cari as $value):?>
						<tr>
							<th scope="row"><?php echo $value['id_pembeli']; ?></th>
							<td><?php echo $value['nama_pembeli']; ?></td>
							<td><?php echo $value['alamat']; ?></td>
							<td><?php echo $value['id_pegawai']; ?></td>
							<td>
								<a href="edit_pembeli.php?id=<?php echo $value['id_pembeli'] ?>" class ="badge badge-warning">Edit</a>
								<a href="hapus_pembeli.php?id=<?php echo $value['id_pembeli'] ?>" class="badge badge-danger">Hapus</a>
								<a href="tambah_pembeli.php?id=" class ="badge badge-warning">Tambah Data</a>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</main>
		</div>
	</div>
</div>

<?php 
	
	include 'layout/footer.php';

?>