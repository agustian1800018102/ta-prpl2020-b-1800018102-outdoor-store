<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';
	$kategori = mysqli_query($koneksi, "SELECT * FROM kategori");
	$pembeli = mysqli_query($koneksi, "SELECT * FROM pembeli");
?>

<div class="container" style="margin-top:40px">
	<h3>Tambah Barang</h3>

	<form method="post" action="proses_barang.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Barang</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="id_barang" placeholder="Masukkan Id ">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Kategori</label>
			<div class="col-sm-10">
				<select class="form-control" name="kategori">
					<?php foreach ($kategori as $valuee): ?>
						<option value="<?=$valuee['id_kategori'];?>">
							<?=$valuee['id_kategori'];?> - <?=$valuee['nama_kategori'];?>
						</option>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Nama Barang</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="nama_barang" placeholder="Masukkan Nama">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Warna</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="warna" placeholder="Masukkan Alamat">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Ukuran</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="ukuran" placeholder="Masukkan Alamat">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Harga</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="harga" placeholder="Masukkan Alamat">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
			<div class="col-sm-10">
				<select class="form-control" name="pembeli">
					<?php foreach ($pembeli as $valuee): ?>
						<option value="<?=$valuee['id_pembeli'];?>">
							<?=$valuee['id_pembeli'];?> - <?=$valuee['nama_pembeli'];?>
						</option>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="col-sm-10">
			<input type="submit" name="submit" class="btn btn-primary" value="SAVE">
			<a href="barang.php" class="btn btn-warning">KEMBALI</a>
		</div>
	</form>
</div>

<?php 
	
	include 'layout/footer.php';

?>