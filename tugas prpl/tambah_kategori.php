<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';
?>

<div class="container" style="margin-top:40px">
	<h3>Tambah Kategori</h3>
	<hr>	
	<table class="table table-striped table-hover table-sm table-bordered">
	<form method="post" action="proses_kategori.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Kategori</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="id_kategori" placeholder="Masukkan Id">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Kategori</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="nama_kategori" placeholder="Masukkan Kategori">
			</div>
		</div>
		<div class="col-sm-10">
			<input type="submit" name="submit" class="btn btn-primary" value="SAVE">
			<a href="Kategori.php" class="btn btn-warning">KEMBALI</a>
		</div>
	</form>
</div>

<?php 
	
	include 'layout/footer.php';

?>