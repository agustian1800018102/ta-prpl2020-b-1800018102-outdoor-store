<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';
?>

<div class="container" style="margin-top:40px">
	<h3>Tambah Pegawai</h3>

	<form method="post" action="proses_pegawai.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pegawai</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="id_pegawai" placeholder="Masukkan Id">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Nama Pegawai</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="nama_pegawai" placeholder="Masukkan nama">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Alamat</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="alamat" placeholder="Masukkan nama">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jabatan</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="jabatan" placeholder="Masukkan nama">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jenis Kelamin</label>
			<div class="col-sm-10 mt-2">
				<input type="radio" name="jenis_kelamin" value="L">Laki Laki
				<input type="radio" name="jenis_kelamin" value="P">Perempuan
			</div>
		</div>
		<div class="col-sm-10">
			<input type="submit" name="submit" class="btn btn-primary" value="SAVE">
			<a href="pegawai.php" class="btn btn-warning">KEMBALI</a>
		</div>
	</form>
</div>

<?php 
	
	include 'layout/footer.php';

?>