<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';
	$pegawai = mysqli_query($koneksi, "SELECT * FROM pegawai");
?>

<div class="container" style="margin-top:40px">
	<h3>Tambah Pembeli</h3>

	<form method="post" action="proses_pembeli.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="id_pembeli" placeholder="...">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Nama</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="nama_pembeli" placeholder="...">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Alamat</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="alamat" placeholder="...">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pegawai</label>
			<div class="col-sm-10">
				<select class="form-control" name="pegawai">
					<?php foreach ($pegawai as $valuee): ?>
						<option value="<?=$valuee['id_pegawai'];?>">
							<?=$valuee['id_pegawai'];?> - <?=$valuee['nama_pegawai'];?>
						</option>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="col-sm-10">
			<input type="submit" name="submit" class="btn btn-primary" value="SAVE">
			<a href="pembeli.php" class="btn btn-warning">KEMBALI</a>
		</div>
	</form>
</div>

<?php 
	
	include 'layout/footer.php';

?>