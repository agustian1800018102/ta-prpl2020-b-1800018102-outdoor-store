<?php 
	
	include 'layout/header.php';

	include 'koneksi.php';

	$barang = mysqli_query($koneksi, "SELECT * FROM barang");
	$pembeli = mysqli_query($koneksi, "SELECT * FROM pembeli");
?>

<div class="container" style="margin-top:40px">
	<h3>Tambah Transaksi</h3>

	<form method="post" action="proses_transaksi.php" class="mt-3">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Transaksi</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="id_transaksi" placeholder="Masukkan Id Transaksi">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Barang</label>
			<div class="col-sm-10">
				<select class="form-control" name="barang">
					<?php foreach ($barang as $value): ?>
						<option value="<?=$value['id_barang'];?>">
							<?=$value['id_barang'];?> - <?=$value['nama_barang'];?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
			<div class="col-sm-10">
				<select class="form-control" name="pembeli">
					<?php foreach ($pembeli as $value): ?>
						<option value="<?=$value['id_pembeli'];?>">
							<?=$value['id_pembeli'];?> - <?=$value['nama_pembeli'];?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Tanggal Pesan</label>
			<div class="col-sm-10">
				<input type="date" name="tgl_pesan" class="form-control">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jumlah</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="jumlah" placeholder="Masukkan jumlah beli">
			</div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Sub Total</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="total" placeholder="Masukkan total bayar">
			</div>
		</div>
		<div class="col-sm-10">
			<input type="submit" name="submit" class="btn btn-primary" value="SAVE">
			<a href="transaksi.php" class="btn btn-warning">KEMBALI</a>
		</div>
	</form>
</div>

<?php 
	
	include 'layout/footer.php';

?>


